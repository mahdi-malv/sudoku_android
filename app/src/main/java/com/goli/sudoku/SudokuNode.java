package com.goli.sudoku;


import java.util.ArrayList;
import java.util.HashSet;

public class SudokuNode {
    private SudokuBoard state;
    private ArrayList<HashSet<Short>> goodValues;

    public SudokuNode(int n) {
        this.state = new SudokuBoard(n);
        this.goodValues = new ArrayList<>();
        for (int i = 0; i < n*n; i++){
            this.goodValues.add(new HashSet<Short>());
        }
    }

    SudokuNode(SudokuBoard state){
        this.state = state;
        int n = this.state.getBoard().length;
        this.goodValues = new ArrayList<>();
        for (int i = 0; i < n*n; i++){
            this.goodValues.add(new HashSet<Short>());
        }
    }

    private SudokuNode(SudokuBoard state, ArrayList<HashSet<Short>> goodValues) {
        this.state = new SudokuBoard(state.getBoard());
        this.goodValues = new ArrayList<>();
        for(HashSet<Short> values : goodValues){
            this.goodValues.add(new HashSet<Short>(values));
        }
    }

    SudokuNode copy(){
        return new SudokuNode(this.state, this.goodValues);
    }
    
    HashSet<Short> getGoodValues(int row, int column) {
        int n = this.state.getBoard().length;
        return this.goodValues.get(row*n+column);
    }

    void assign(int row, int col, short value){
        this.state.assign(row, col, value);
        this.getGoodValues(row, col).clear();
        this.getGoodValues(row, col).add(value);
    }

    SudokuBoard getState() { return this.state; }
    public ArrayList<HashSet<Short>> getAllGoodValues() { return this.goodValues; }
}