package com.goli.sudoku;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TableLayout tableLayout = findViewById(R.id.t);
        TextView tv1 = findViewById(R.id.tv1);
        TextView tv2 = findViewById(R.id.tv2);

//        String testBoard = "0200000203404000";
        String testBoard = "000020040008035000000070602031046970200000000000501203049000730000000010800004000";
        SudokuBoard questionBoard = new SudokuBoard(testBoard);

        // Start timer
        long start = System.currentTimeMillis();
        SudokuBoard answerBoard = SudokuUtil.solve(testBoard);
        // End timer
        long timeTaken = System.currentTimeMillis() - start;

        tv1.setText("Solved in " + timeTaken + " (ms)");
        tv2.setText("Question was:\n" + questionBoard.toString());


        if (answerBoard == null) {
            Toast.makeText(this, "Failed to solve board", Toast.LENGTH_SHORT).show();;
            return;
        }
        createTableLayout(
                tableLayout,
                answerBoard.getBoard(),
                questionBoard.getBoard());
    }

    private void createTableLayout(TableLayout t, short[][] answer, short[][] question) {
        for (int i = 0; i < answer.length; i++) {
            TableRow r = new TableRow(this);
            r.setGravity(Gravity.CENTER);
            for (int j = 0; j < answer[i].length; j++) {
                TextView v = (TextView) LayoutInflater.from(this).inflate(R.layout.text_row, null);
                String textToDisplay = " " + answer[i][j] + " ";

                if (question[i][j] == answer[i][j]) {
                    // The cell is for question
                    v.setBackgroundColor(Color.parseColor("#000000"));
                    v.setTextColor(Color.parseColor("#858585"));
                }

                v.setText(textToDisplay);
                r.addView(v);
                System.out.println("Boaring row: " + i + " and col: " + j);
            }
            t.addView(r);
        }
    }
}