package com.goli.sudoku;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

@SuppressWarnings("WeakerAccess")
public class SudokuUtil {

    public static void initUnaryConsitentValues(SudokuNode node, String boardString) {
        int n = node.getState().getBoard().length;
        int k = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                short v = (short) Character.getNumericValue(boardString.charAt(k++));
                if (v == 0) {
                    for (v = 1; v <= n; v++) {
                        node.getGoodValues(i, j).add(v);
                    }
                } else {
                    node.getGoodValues(i, j).add(v);
                }
            }
        }
    }

    /**
     * Uses MRV to get unassigned var
     */
    public static SudokuItem getUnassignedVar(SudokuNode node) {
        int n = node.getState().getBoard().length;
        // [start:1]
        boolean checkFirst = true;
        int row = 0, col = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (node.getState().getBoard()[i][j] == 0) {
                    if (checkFirst) {
                        row = i;
                        col = j;
                        checkFirst = false;
                    } else {
                        if (node.getGoodValues(i, j).size() < node.getGoodValues(row, col).size()) {
                            row = i;
                            col = j;
                        }
                    }
                }
            }
        }
        return new SudokuItem(row, col);
    }

    public static ArrayList<Short> getDomainValues(SudokuNode node, SudokuItem var) {
        // return order the the hashset
        return new ArrayList<>(node.getGoodValues(var.R(), var.C()));
    }

    public static HashSet<SudokuItem> getConnectedOpenVariables(SudokuNode node, SudokuItem var, int n) {
        int m = (int) Math.sqrt(n);
        short[][] board = node.getState().getBoard();
        HashSet<SudokuItem> vars = new HashSet<>();
        for (int i = 0; i < n; i++) {
            if (board[i][var.C()] == 0) {
                vars.add(new SudokuItem(i, var.C()));
            }
            if (board[var.R()][i] == 0) {
                vars.add(new SudokuItem(var.R(), i));
            }
            vars.add(new SudokuItem(var.R(), i));
        }
        int boxRow = var.R() - var.R() % m;
        int boxCol = var.C() - var.C() % m;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                if (board[boxRow + i][boxCol + j] == 0) {
                    vars.add(new SudokuItem(boxRow + i, boxCol + j));
                }
            }
        }
        vars.remove(var);
        return vars;
    }

    public static boolean enforceAC(SudokuNode node, SudokuItem head, SudokuItem tail) {
        // enforce arc consistency, return true if modified available values of the tail
        boolean modified = false;
        // [start:2]
        ArrayList<Short> valueSetHead = getDomainValues(node, head);

        // Concentrate SudokuItem that has only 1 domain left
        if (valueSetHead.size() == 1) {
            short headVal = valueSetHead.get(0);
            ArrayList<Short> valueSetTail = getDomainValues(node, tail);
            // remove the value in tail that cause the conflict
            if (valueSetTail.contains(headVal)) {
                node.getGoodValues(tail.R(), tail.C()).remove(headVal);
                modified = true;
            }
        }
        // [end:2]
        return modified;
    }

    public static boolean forwardCheck(SudokuNode node, SudokuItem var) {
        int n = node.getState().getBoard().length;
        // forward checking enforce immediate arcs and returns true if it is still solvable
        // [start:3]
        boolean solvable = true;
        HashSet<SudokuItem> neighborSet = getConnectedOpenVariables(node, var, n);

        for (SudokuItem varTail : neighborSet) {
            if (enforceAC(node, new SudokuItem(var.R(), var.C()), varTail)) {
                if (getDomainValues(node, varTail).size() == 0) {
                    solvable = false;
                }
            }
        }

        return solvable;
        //throw new UnsupportedOperationException("Forward Checking has not been implemented.");
        // [end:3]
    }

    public static boolean inference(SudokuNode node, SudokuItem var) {
        if (!INFER) {
            return true;
        }
        return forwardCheck(node, var);
    }

    public static SudokuNode backtrackSearch(SudokuNode node, int depth) {
        // backtrack search
        // return null if not current node is not solvable
        // otherwise return solved node
        // useful code:
        // - node.getState().isFilled(): return true if completed assignment
        // - node.getState().isSolved(): return true if valid assignment
        // - node.copy(): return a new node.
        //                It is important to copy a node before you assign or recurse
        // - node.assign(row, col, value): assign value to a position. This also sets
        //                                 the possible values of the position to be the value.
        // - if (depth > LIMIT) {return null;}: useful to stop early
        // [start:0]

        // base case, if it is solved, return that node.
        if (node.getState().isSolved()) {
            return node;
        }
        if (depth > LIMIT || node.getState().isFilled()) {
            return null;
        }
        // select unassigned variable
        SudokuItem newVar = getUnassignedVar(node);
        // get the good values for that var
        ArrayList<Short> newValueSet = getDomainValues(node, newVar);
        for (Short val : newValueSet) {
            SudokuNode newNode = node.copy();
            // assign val into assignment
            newNode.assign(newVar.R(), newVar.C(), val);
            depth++;
            // check inference
            if (inference(newNode, newVar)) {
                SudokuNode finalNode = backtrackSearch(newNode, depth);
                if (finalNode != null) {
                    return finalNode;
                }
            }
            // remove var from assignment
            newNode.getState().unassign(newVar.R(), newVar.C());
        }


        // [end:0]

        return null;
    }

    public static SudokuBoard solve(String boardString) {
        int n = (int) Math.sqrt(boardString.length());
        SudokuNode root = new SudokuNode(new SudokuBoard(n));
        initUnaryConsitentValues(root, boardString);
        SudokuNode finalNode = backtrackSearch(root, 0);
        if (finalNode != null) return finalNode.getState();
        else return null;

    }

    public static int LIMIT = 9 * 9 + 1;  // helpful for debugging
    public static boolean INFER = true;

    public static void main(String[] args) {
//        String testBoard = "0200000203404000";
        String testBoard = "000020040008035000000070602031046970200000000000501203049000730000000010800004000";
        Date startTime = new Date();
        SudokuBoard solved = solve(testBoard);
        Date endTime = new Date();
        if (solved != null) {
            System.out.println(solved);
            System.out.println("Solved in " + (endTime.getTime() - startTime.getTime()) + "ms");
        }
    }

}
