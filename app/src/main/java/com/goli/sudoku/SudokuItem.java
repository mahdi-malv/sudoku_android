package com.goli.sudoku;


/**
 * Representing a single cell
 */
public class SudokuItem {
    private int row;
    private int col;
    SudokuItem(int row, int col) {
        this.row = row;
        this.col = col;
    }
    int R(){return this.row;}
    int C(){return this.col;}

    public String toString() {
        return "(" + this.row + ", " + this.col + ")";
    }

    @Override
    public int hashCode(){
        return this.toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!SudokuItem.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        final SudokuItem other = (SudokuItem) obj;
        return this.row == other.row && this.col == other.col;
    }
}